# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.3'

gem 'awesome_rails_console'
gem 'aws-sdk-s3', require: false
gem 'bootsnap', '>= 1.4.2', require: false
gem 'devise'
gem 'faraday'
gem 'figaro'
gem 'hotwire-rails'
gem 'image_processing', '~> 1.2'
gem 'jbuilder', '~> 2.5'
gem 'meta-tags'
gem 'pg'
gem 'puma', '~> 4.3.0'
gem 'rails', '~> 6.1.3'
gem 'redis-rack-cache'
gem 'sass-rails', '>= 6'
gem 'sidekiq'
gem 'webpacker', '~> 5.0'

group :development, :test do
  gem 'factory_bot_rails' # Fake object generation
  gem 'faker' # Fake test data
  gem 'hirb'
  gem 'hirb-unicode-steakknife', require: 'hirb-unicode'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
end

group :development do
  gem 'foreman'
  gem 'listen', '>= 3.0.5', '< 3.3'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'rspec-rails'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end
