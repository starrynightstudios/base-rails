# Base Rails

This is a basic Rails template with the following libraries installed:

- [Ruby 2.6.x](.ruby-version)
- [Rails 6.x](Gemfile)
- [Postgres](https://www.postgresql.org/)
- [Redis](https://redis.io/)
- [Sidekiq](https://github.com/mperham/sidekiq)
- [Devise](https://github.com/plataformatec/devise)
- [Bulma](https://bulma.io)
- [Rspec](https://rspec.info/)
- [Faraday](https://github.com/lostisland/faraday)
- [Stimulus](https://stimulusjs.org/)
- [FontAwesome](https://fontawesome.com)
- [Figaro](https://github.com/laserlemon/figaro)
- [Heroku Procfile](https://devcenter.heroku.com/articles/procfile)
- [Rubocop](https://github.com/rubocop-hq/rubocop)

## Installation

Do a find and replace of `BaseRails` and `base_rails` to change to your app's
name.

### Prerequistes

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install)
- `rails webpacker:install`

### Install

- `bin/setup`

## Background Workers

By default, background workers are enabled and run via Sidekiq. If you'd like
to disable background workers and have everything run synchronously you can
disable background workers by creating an empty file at `tmp/disable-queuing.txt`.
This only affects the development environment.
