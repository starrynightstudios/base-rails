# frozen_string_literal: true

module ApplicationHelper
  ##
  # Translates a flash message key into a Bulma-friendly
  # is-class variable
  #
  # @see https://bulma.io/documentation/elements/notification/
  #
  def bulma_flash_class(key)
    case key.to_sym
    when :alert
      :warning
    when :error
      :danger
    when :notice
      :success
    else
      key
    end
  end
end
